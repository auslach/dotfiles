execute pathogen#infect()
filetype on " Automatically detect file types
filetype plugin indent on " Automatically detect file types
set sessionoptions-=options
set nocompatible " We don't want vi compatibility
syntax enable
set background=dark
" let g:solarized_termcolors=256
" let g:solarized_visibility = "high"
" let g:solarized_contrast = "high"
colorscheme solarized

" Load Ctrl-P
set runtimepath^=~/.vim/bundle/ctrlp.vim

" Set leader key to ','
let mapleader=","

" Set paste and no paste
command Refresh windo e
command Syp set paste
command Snp set nopaste

" change movement from Ctrl-W + J to Ctrl-J
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" disable middle-mouse button pasting
nnoremap <MiddleMouse> <Nop>
nnoremap <2-MiddleMouse> <Nop>
nnoremap <3-MiddleMouse> <Nop>
nnoremap <4-MiddleMouse> <Nop>

inoremap <MiddleMouse> <Nop>
inoremap <2-MiddleMouse> <Nop>
inoremap <3-MiddleMouse> <Nop>
inoremap <4-MiddleMouse> <Nop>

" Set splits on bottom & right hand side
set splitbelow
set splitright

" Change which file opens after executing :Rails command
let g:rails_default_file='config/database.yml'

set ts=2  " Tabs are 2 spaces
set bs=2  " Backspace over everything in insert mode
set shiftwidth=2  " Tabs under smart indent
set nocp incsearch
set cinoptions=:0,p0,t0
set cinwords=if,else,while,do,for,switch,case
set formatoptions=tcqr
set cindent
set autoindent
set smarttab
set expandtab
set autoread
set number " Show line numbers
" set color of column 80
set colorcolumn=80
" super awesome tab completion in vim
set wildmenu

" Visual
set showmatch  " Show matching brackets.
set mat=5  " Bracket blinking.
set nolist
set novisualbell  " No blinking .
set noerrorbells  " No noise.
set laststatus=2  " Always show status line.

set tags+=gems.tags

" Add JBuilder syntax highlighting (ruby)
au BufNewFile,BufRead *.json.jbuilder set ft=ruby

" Highlight current line
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END

" Show whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

" Disable json quote concealing
let g:vim_json_syntax_conceal = 0
