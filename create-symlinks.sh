#!/bin/bash
###############################
#  .create-symlinks.sh
#  Create symlinks from home directory to files/folders in ~/dotfiles
#  Taken & modified from http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/
###############################

### Variables #################

# dotfiles directory
dir=~/dotfiles

# old dotfiles backup directory
olddir=~/dotfiles_old

# list of files/folders to symlink
files=("gemrc" "gitignore" "oh-my-zsh" "tmux.conf" "vim" "vimrc" "zshenv" "zshrc")

###############################

# create dotfiles_old in home directory
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in home directory to dotfiles_old directory
# then create symlinks
for file in $files; do
	echo "Moving any existing dotfiles from ~ to $olddir"
	mv ~/.$file $olddir
	echo "Creating symlink to $file in home directory"
	ln -s $dir/$file ~/.$file
done
